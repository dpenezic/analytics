<!--->
This issue is for data modeling needs. If you need access to a table or field, or need a new calculated value within the database or Looker, use this template. 
<---->
#### What is the data source? Is it already in the Data Warehouse?

SFDC? ZenDesk? etc.

#### What is the problem you're trying to solve?


/label ~Modeling